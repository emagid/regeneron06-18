<?php

namespace Model;

class Highlight extends \Emagid\Core\Model {
    static $tablename = "public.highlight";

    public static $fields  =  [
        'image',
        'display_order' => ['type'=>'numeric'],
    ];

}