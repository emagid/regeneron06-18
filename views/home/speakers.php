<main class='content'>
	<a class='home_btn' href="/"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>
	<main class='background'>
		<section class="speakers">
	        <div class='cards logos'>
	            <div data-id='sana' class='card half'>
	                <div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/sana.jpg')"></div>
	                <p>Sana Rahim 
	            		<span>People Operations Manager</span>
	            	</p>
	            </div>
	            <div data-id='mike' class='card half'>
	            	<div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/mike.jpg')"></div>
	            	<p>Mike Powell 
	            		<span>Olympic Medalist, Long Jump</span>
	            	</p>
	            </div>
	            <div data-id='claudio' class='card'>
	            	<div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/claudio.jpg')"></div>
	            	<p>Claudio Diaz 
	            		<span>Human Capital Executive Consultant</span>
	            	</p>
	            </div>
	            <div data-id='dan' class='card'>
	            	<div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/dan.jpg')"></div>
	            	<p>Dan Fagan 
	            		<span>Human Resources Research</span>
	            	</p>
	            </div>
	            <div data-id='holly' class='card'>
	            	<div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/holly.jpg')"></div>
	            	<p>Holly Tassi 
	            		<span>Brand and Marketing Consultant</span>
	            	</p>
	            </div>
	            <div data-id='kathi' class='card'>
	            	<div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/kathi.jpg')"></div>
	            	<p>Kathi Enderes 
	            		<span>VP Talent and Workforce Research Leader at Bersin, Deloitte Consulting LLP</span>
	            	</p>
	            </div>
	            <div data-id='matthew' class='card'>
	            	<div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/matthew.jpg')"></div>
	            	<p>Matthew P. Tuller, Esq.
	            		<span>Former Vice President Global Supply Management at Jacobs</span>
	            	</p>
	            </div>
	            <div data-id='sean' class='card'>
	            	<div class='pic' style="background-image: url('<?= FRONT_ASSETS ?>img/speakers/sean.jpg')"></div>
	            	<p>Sean Ring 
	            		<span>Co-Founder, Fulcrum</span>
	            	</p>
	            </div>
	        </div>
	        <div class='popup'>
	        	<div class='offclick'></div>
	        	<div class='holder'>
	        		<h4 class='close'>x</h4>
		        	<img src="">
		            <p></p>
	        	</div>
	        </div>
	    </section>
	</main>

	<script type="text/javascript">
	    logos = {
	        'sana': {
	            p: 'Sana Rahim is a people operations manager at Freckle Education, an education technology company. As Freckle’s first HR leader, Sana builds the people and culture function – and thinks critically about how to cultivate an environment where employees are happy and able to do their best work. In her role, she oversees diversity and inclusion, talent development, and performance management. Sana is also the founder and CEO of Emerge Consulting, a management practice that builds the capacity of leaders and organizations to create more diverse and inclusive environments.'
	        },
	        'mike': {
	            p: 'Mike Powell is a long jumper who has competed at three Olympic Games (1988-96), winning silver medals in 1988 and 1992, behind Carl Lewis. He is best known for winning the greatest long jump competition in the history of the sport, when he defeated Lewis at the 1991 World Championships in Tokyo. <br><br>At that meet, Powell jumped 8.95 metres (29-4 ½") to break the vaunted world record set by Bob Beamon at the 1968 Olympics. To secure the gold medal and the world record, Powell went head to head with Carl Lewis and won. He would go on to win another gold at the 1993 World Championships and a silver at the 1995 Worlds.<br><br>For his 1991 world record Powell was named the Sullivan Award Winner in the US and selected as the BBC Overseas Sports Personality of the Year. He later became a coach, teaching at the Academy of Speed in Rancho Cucamonga, California.'
	        },
	        'claudio': {
	            p: 'Claudio Diaz is a Human Capital Executive Consultant who helps CEOs revitalize their cultures while creating profitable growth, designing ideal governance models, and enhancing “people engagement.”<br><br>For the past decade and a half, Claudio has served as chief human capital officer for two of the nation’s top 20 accounting firms where he led HR professionals in designing, developing, and delivering cultures of excellence. In addition, he has worked as the corporate senior consultant for a large hospital system and as the director of leadership and organizational development for a global manufacturing firm where he leveraged Six Sigma concepts to ensure optimal output for both his HR teams as well as plant operations.<br><br>Claudio has served on both Boards for Goodwill Industries and United Way, and was the Founding President of the Latino Professionals Association of Greater Madison.'
	        },
	        'dan': {
	            p: "Dan Fagan is passionate about workforce development and likes to work at the intersection of adult education and economics to understand how incentives impact adults' ability to learn and earn in our changing global economy. As a former HR executive, Dan has led successful HR transformations for global Fortune 500 companies and is now focused on the 'supply side' of the talent value chain. Dan’s research interests concern improved understanding and outcomes in workforce resiliency and sustainability, with specific focus on workforce entry points.<br><br>Dan studies postgraduate Economics at the University of Missouri and Educational Leadership, Policy, and Human Development at North Carolina State University."
	        },
	        'holly': {
	            p: "Holly Tassi is a brand and marketing consultant that is passionate about creating products that are better-for-you and better for the planet. She is the co-founder and CEO of Believe in Bambara, a social enterprise popularizing an underutilized crop sourced directly from small holder female farmers in Ghana. Holly also works with Kuli Kuli Foods, a certified B-corporation on a mission to improve nutrition and livelihoods worldwide with a superfood called moringa.<br><br>Holly started her career in supply and operations with Diageo, one of the world’s largest adult beverage companies. She spent three years with Best Friends Animal Society, a nationwide non-profit animal rescue and advocacy organization.<br><br>Hollys holds an MBA from the Marshall School of Business at the University of Southern California along with a Bachelor’s of Science in Supply Chain Management and Public Relations from Syracuse University."
	        },
	        'kathi': {
	            p: "Kathi Enderes leads talent and workforce research for Bersin, Deloitte Consulting LLP, enabling organizations to transform work and the worker experience for increased organizational performance. With more than 20 years global human capital experience from management consulting with IBM, PwC and EY and as a talent management leader in large complex organizations, she specializes talent strategies, talent development and management, performance management, and change management. Kathi is passionate about helping organizations transition to the future of work. She holds a doctoral degree in mathematics and a master’s degree in mathematics from the University of Vienna, Austria."
	        },
	        'matthew': {
	            p: "Matthew Tuller, Esquire has led the transformation of Jacobs Global Supply Management function into a best in class team delivering the highest quality services to their internal and external clients. Prior to that, Matt served as the chief procurement officer (CPO) for CH2M Hill, leading the transformation of the contracts and procurement groups from a decentralized group into a high-functioning corporate team utilizing automation, technology, and a global shared service center to drive innovation.<br><br>Prior to taking the CPO role, Matt held various positions within the general counsel’s office of CH2M, including serving as chief operations counsel and handling global litigation matters. Matt was a successful attorney prior to joining CH2M, and brought the same drive for efficiency, innovative tools, and focus on results for his private clients to solving the problems of today’s corporate organizations."
	        },
	        'sean': {
	            p: "In January 2018, Sean Ring co-founded Fulcrum, a software platform that aggregates and integrates OnDemand talent clouds into the enterprise. Fulcrum is a first mover in this space, delivering an end to end OnDemand talent platform with enterprise grade compliance. In his role as CRO, Sean supports Fortune 1000 business leaders as they prepare their organizations for a future of work that looks much different from today.<br><br>Prior to Fulcrum, Sean spent five years in a business development capacity with Innovative Employee Solutions, a 40-year-old IC compliance and payrolling company. Sean holds CCWP accreditation from Staffing Industry Analysts, and was recognized as a top millennial in the staffing industry by Staffing Industry Review Magazine.<br><br>Sean thrives on new experiences and is passionate about the future of work. He has three simple mantras in life and in business: Don’t talk about it, be about it; always look forward and up; and water your garden."
	        }
	    }

	    $(document).on('click', '.card', function(){
	    	$(this).addClass('enlarge');
	        $(this).addClass('open');
	        var id = $(this).attr('data-id');
            $('.popup p').html(logos[id].p);
	        var timer = setTimeout(function(){
	        	$(this).removeClass('enlarge');
	            $('.popup').fadeIn(300);
	            $('.popup').css('display', 'flex');
	        }, 500);
	    });

	    $(document).on('click', '.offclick, .close', function(){
	    	$('.popup').fadeOut(300);
	    });
	</script>
</main>