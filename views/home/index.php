<main class='content'>
	<div class='home'>
		<div class='touch'>
			<p>Touch below to get started!</p>
			<img src="<?= FRONT_ASSETS ?>img/hand.png">
		</div>
		<div class='cards'>
			<img class='full' src="<?= FRONT_ASSETS ?>img/card1.png">
			<img id='dashboard' src="<?= FRONT_ASSETS ?>img/card2.png">
			<img src="<?= FRONT_ASSETS ?>img/card3.png">
			<img class='full' src="<?= FRONT_ASSETS ?>img/card4.png">
		</div>
	</div>
</main>