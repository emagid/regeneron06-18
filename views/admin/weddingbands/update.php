<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->weddingBand->id; ?>"/>
    <input type="hidden" name="colors" value="<?php echo $model->weddingBand->colors; ?>"/>
    <input type="hidden" name="active" value="1"/>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab"
                                                      data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
            <li role="presentation"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a>
            </li>

            <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

<!--                            <div class="form-group">-->
<!--                                <label>Featured image</label>-->
<!--                                <p>-->
<!--                                    <small>(ideal featured image size is 500 x 300)</small>-->
<!--                                </p>-->
<!--                                <p><input type="file" name="featured_image" class='image'/></p>-->
<!---->
<!--                                <div style="display:inline-block">-->
<!--                                    <div class='preview-container'></div>-->
<!--                                </div>-->
<!--                            </div>-->

                            <div class="form-group">
                                <label>Featured Video</label>
                                <p>
                                    <small>(ideal featured video is less then 10M)</small>
                                </p>
                                <p><input type="file" name="video_link" class='video'/></p>
                                <?php if($model->weddingBand->video_link) { ?>
                                <div style="display:inline-block">
                                    <video controls width="400" height="300">
                                        <source src="<?=$model->weddingBand->videoLink()?>" type="video/mp4">
                                    </video>
                                    <div class='preview-container'><?=$model->weddingBand->video_link?></div>
                                </div>
                                <? } ?>
                            </div>

                            <div class="form-group">
                                <label>Alias</label>
                                <?php echo $model->form->editorFor("alias"); ?>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>
                            <div class="form-group">
                                <label>Setting Style</label>
                                <?php echo $model->form->editorFor("setting_style"); ?>
                            </div>
                            <div class="form-group">
                                <label>Diamond Color</label>
                                <?php echo $model->form->editorFor("diamond_color"); ?>
                            </div>
                            <div class="form-group">
                                <label>Diamond Quality</label>
                                <?php echo $model->form->editorFor("diamond_quality"); ?>
                            </div>
                            <div class="form-group">
                                <label>Metals -> Colors</label>
                                <?$metal_colors = json_decode($model->weddingBand->metal_colors);
                                foreach($metal_colors as $metal=>$colors){?>
                                    <div class="checkbox">
                                        <label>
                                            <input class="metal" type="checkbox" value="<?=$metal?>" name="metals[]" <?=strpos($model->weddingBand->metals, $metal) === false?'':'checked'?>><?=$metal?>
                                            <?foreach($colors as $color=>$val){?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="color" type="checkbox" value="<?=$color?>" name="<?=$metal?>_colors[]" <?=$val == 0 ?'':'checked'?>><?=$color?>
                                                    </label>
                                                </div>
                                            <?}?>
                                        </label>
                                    </div>
                                <?}?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Prices</h4>
                            <?foreach($model->weddingBand->getOptions() as $stoneBreak => $option){ ?>
                            <h5>For <?=$stoneBreak?></h5>
                            <div class="form-group">
                                <label>14KT Price</label>
                                <input readonly type="text" value="<?=round($option['price']['14kt_gold'],2)?>"/>
                            </div>
                            <div class="form-group">
                                <label>18KT Price</label>
                                <input readonly type="text" value="<?=round($option['price']['18kt_gold'],2)?>"/>
                            </div>
                            <div class="form-group">
                                <label>Platinum</label>
                                <input readonly type="text" value="<?=round($option['price']['Platinum'],2)?>"/>
                            </div>
                            <? } ?>
                            <input type="hidden" name="options" value='<?=$model->weddingBand->options?>'>
                            <input type="hidden" name="options" value='<?=$model->weddingBand->price?>'>
                            <div class="form-group">
                                <label>Wholesale Price</label>
                                <?php echo $model->form->editorFor("wholesale_price"); ?>
                            </div>
                            <div class="form-group">
                                <label>MSRP</label>
                                <?php echo $model->form->editorFor("msrp"); ?>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label>MSRP Enabled?</label>
                                <?php echo $model->form->checkBoxFor("msrp_enabled", 1); ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Expenses</h4>
                            <div class="form-group">
                                <label>Setting Labor</label>
                                <?php echo $model->form->editorFor("setting_labor"); ?>
                            </div>
                            <div class="form-group">
                                <label>Polish Labor</label>
                                <?php echo $model->form->editorFor("polish_labor"); ?>
                            </div>
                            <div class="form-group">
                                <label>Head to Add</label>
                                <?php echo $model->form->editorFor("head_to_add"); ?>
                            </div>
                            <div class="form-group">
                                <label>Melee Cost</label>
                                <?php echo $model->form->editorFor("melee_cost"); ?>
                            </div>
                            <div class="form-group">
                                <label>Shipping from Casting</label>
                                <?php echo $model->form->editorFor("shipping_from_cast"); ?>
                            </div>
                            <div class="form-group">
                                <label>Shipping to Customer</label>
                                <?php echo $model->form->editorFor("shipping_to_customer"); ?>
                            </div>
                            <div class="form-group">
                                <label>Total 14kt Cost</label>
                                <!--                        --><?php //echo $model->form->editorFor("total_14kt_cost"); ?>
                                <input readonly type="text" name="total_14kt_cost" value="<?=round($model->weddingBand->total_14kt_cost,2)?>"/>
                            </div>
                            <div class="form-group">
                                <label>Total 18kt Cost</label>
                                <!--                        --><?php //echo $model->form->editorFor("total_18kt_cost"); ?>
                                <input readonly type="text" name="total_18kt_cost" value="<?=round($model->weddingBand->total_18kt_cost,2)?>"/>
                            </div>
                            <div class="form-group">
                                <label>Total Platinum Cost</label>
                                <!--                        --><?php //echo $model->form->editorFor("total_plat_cost"); ?>
                                <input readonly type="text" name="total_plat_cost" value="<?=round($model->weddingBand->total_plat_cost,2)?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="seo-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label>Tags</label>-->
<!--                                --><?php //echo $model->form->editorFor("tags"); ?>
<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>


            <div role="tabpanel" class="tab-pane" id="categories-tab">
                <select name="weddingband_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
                    <?php foreach ($model->categories as $cat) { ?>
                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                    <?php } ?>
                </select>
            </div>


            <div role="tabpanel" class="tab-pane" id="images-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <div class="dropzone" id="dropzoneForm"
                                 action="<?php echo ADMIN_URL . 'weddingbands/upload_images/' . $model->weddingBand->id; ?>">
                            </div>
                            <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <table id="image-container" class="table table-sortable-container">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>File Name</th>
                                    <th>Display Order</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? $prod = new \Model\Product_Image() ?>
                                <?php foreach ($prod->getProductImage($model->weddingBand->id, "WeddingBand") as $img) {

                                    if ($img->exists_image()) {
                                        ?>
                                        <tr data-image_id="<?php echo $img->id; ?>">
                                            <td><img src="<?php echo $img->get_image_url(); ?>" width="100"
                                                     height="100"/></td>
                                            <td><?php echo $img->image; ?></td>
                                            <td class="display-order-td"><?php echo $img->display_order; ?></td>
                                            <td class="text-center">
                                                <a class="btn-actions delete-product-image"
                                                   href="<?php echo ADMIN_URL; ?>products/delete_prod_image/<?php echo $img->id; ?>?token_id=<?php echo get_token(); ?>">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript">

    var categories = <?php echo json_encode($model->weddingband_category); ?>;
    var site_url =<?php echo json_encode(ADMIN_URL.'products/'); ?>;
    $(document).ready(function () {
        //var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });


//        $("input[name='name']").on('keyup', function (e) {
//            var val = $(this).val();
//            val = val.replace(/[^\w-]/g, '-');
//            val = val.replace(/[-]+/g, '-');
//            $("input[name='slug']").val(val.toLowerCase());
//        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='weddingband_category[]']").val(categories);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-product-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post('/admin/weddingbands/sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            }
        });
        $('.metal').on('change', function(){
            if($(this).is(':checked')) {
                $(this).siblings().find('.color').prop('checked', true);
            } else {
                $(this).siblings().find('.color').prop('checked', false);
            }
        });
        $('.color').on('change', function(){
            if($(this).is(':checked')){
                $(this).parent().parent().siblings('.metal').prop('checked',true);
            }
        });

    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'weddingbands/upload_images/'.$model->weddingBand->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>