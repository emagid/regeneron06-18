<?php
 if(count($model->surveys)>0){ ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="15%">Id</th>
            <th width="15%">Answer</th>
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->surveys as $obj){ ?>
        <tr>
            <td><a href="#"><?php echo $obj->id; ?></a></td>
            <td><a href="#"><?php echo $obj->answer; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>surveys/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } else { ?>
    <p>No Surveys taken yet! </p>
<? }
?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'surveys';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>